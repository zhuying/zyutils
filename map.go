package zyutils

// MapKeys 获取map keys
func MapKeys[T any](m map[string]T) []string {
	keys := make([]string, len(m))
	index := 0
	for key := range m {
		keys[index] = key
		index++
	}
	return keys
}

// MapKeys 获取map keys
func MapValues[T any](m map[string]T) []T {
	values := make([]T, len(m))
	index := 0
	for _, val := range m {
		values[index] = val
		index++
	}
	return values
}
