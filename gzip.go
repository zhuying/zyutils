package zyutils

import (
	"bufio"
	"compress/gzip"
	"log"
	"os"
)

type ReadGzipFile struct {
	ReadFile
	reader *gzip.Reader
}

func (f *ReadGzipFile) Open(file Path) {
	var err error
	f.handler, err = os.Open(string(file))
	if err != nil {
		log.Panicln(err)
	}
	f.reader, err = gzip.NewReader(f.handler)
	if err != nil {
		log.Panicln(err)
	}
	f.scanner = bufio.NewScanner(f.reader)
}

func (f ReadGzipFile) Close() {
	err := f.handler.Close()
	if err != nil {
		log.Panic(err)
	}
	err = f.reader.Close()
	if err != nil {
		log.Panic(err)
	}
}

func NewReadGzipFile[T string | Path](file T) ReadGzipFile {
	f := ReadGzipFile{}
	f.Open(Path(file))
	return f
}
