package zyutils

import (
	"log"
	"os"
)

type Path string

func (p Path) Exists() bool {
	_, err := os.Stat(string(p))
	return os.IsNotExist(err)
}

func (p Path) IsDir() bool {
	stat, _ := os.Stat(string(p))
	return stat.IsDir()
}

func (p Path) IsFile() bool {
	stat, _ := os.Stat(string(p))
	return !stat.IsDir()
}

func (p Path) CreateDir(exists bool) {
	if p.Exists() {
		if !p.IsDir() {
			log.Panicf("%s exists and it's not a directory", p)
		}
	} else {
		err := os.MkdirAll(string(p), os.ModePerm)
		if err != nil {
			log.Panic(err)
		}
	}
}

func (p Path) Remove() {
	err := os.RemoveAll(string(p))
	if err != nil {
		log.Panic(err)
	}
}

func (p Path) Rename(n Path) {
	err := os.Rename(string(p), string(n))
	if err != nil {
		log.Panic(err)
	}
}
